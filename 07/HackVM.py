#!/usr/bin/env python3

"""
  HackVM.py - VM translator for the HACK computer architecture.
  Written by: Joseph Nahmias <joe@nahmias.net>
  For more info, see: https://www.nand2tetris.org/project07
"""

import argparse
import logging
import random
import re
import sys
from pathlib import Path

def get_args():
    """
    Function to parse the command-line arguments according to the
    following rules:
        1) Allow output filename to be specified using -o.
        2) When -o is specified, it must be followed by string to be used as
        the appropriate filename.
        3) If -o is not specified, output filename should default to same as
        the input with the suffix .asm changed/added.
    """
    argp = argparse.ArgumentParser(
        allow_abbrev=False,
        description='VM Translator for the HACK computer.',
    )
    argp.add_argument(
        '-v', '--verbose',
        action='count', default=0,
        help=('increase log verbosity'),
    )
    argp.add_argument(
        'infile',
        metavar='hack_prog.vm',
        type=argparse.FileType('r'),
        help='name of file to read containing Hack VM instructions',
    )
    argp.add_argument(
        '-o', '--out',
        metavar='hack_prog.asm',
        type=argparse.FileType('w+'),
        help='name of file to write hack assembly language translation'
            + ' [default: change filename extension from .vm to .asm]',
    )
    argp.add_argument(
        '--logfile',
        metavar='hack_prog.log',
        type=argparse.FileType('w'),
        default=sys.stderr,
        help=('send log messages to the designated file [default: stderr]'),
    )
    return vars(argp.parse_args())


class Code:
    """Class to generate Hack ASM code from Hack VM instructions"""
    logger = logging.getLogger('Code')

    def __init__(self, outfile):
        self._f = outfile
        self._lbl_num = 0       # Counter to generate unique labels

    _op = {             # Stack Logic & Arithmetic commands
        'neg': '-M', 'not': '!M',
        'add': 'D+M', 'sub': 'M-D', 'and': 'D&M', 'or': 'D|M',
        'eq': 'JEQ', 'gt': 'JGT', 'lt': 'JLT'
    }

    _seg_addr = {       # Memory segment base addresses
        'local': '@LCL', 'argument': '@ARG',
        'this': '@THIS', 'that': '@THAT',
        'constant': None, 'static': '@16',
        'temp': '@5', 'pointer': '@3',
    }

    _seg_reg = {        # Register to use for each memory segment
        'M': ['local', 'argument', 'this', 'that'],
        'A': ['static', 'temp', 'pointer'],
    }
    _reg = { seg: reg for reg, segs in _seg_reg.items() for seg in segs }

    def out(self, cmd, **args):
        '''function to generate HACK assembly code for the given VM
        command and arguments and write it to the output file.'''

        # unpack kw args
        seg = args.get('seg')
        i = args.get('i')

        # lookup model info
        op = Code._op.get(cmd)
        base = Code._seg_addr.get(seg)
        reg = Code._reg.get(seg)

        # Unary operators
        if cmd in ['neg', 'not']:
            return self._f.write(
                # A = *SP - 1;
                '@SP\n'
                'A=M-1\n'
                # *A = op;
                f'M={op}\n'
            )

        # Binary arithmetic & logic operators
        if cmd in ['add', 'sub', 'and', 'or']:
            return self._f.write(
                # A = --*SP;
                '@SP\n'
                'AM=M-1\n'
                # D = *A;
                'D=M\n'
                # A--;
                'A=A-1\n'
                # *A = op;
                f'M={op}\n'
            )

        # Comparison operators
        if cmd in ['gt', 'eq', 'lt']:
            # In HackVM: false == 0; true = -1;
            self._lbl_num += 1      # Increment auto-generated label number
            return self._f.write(
                # A = --*SP; D = *A; A--; D = *A - D;
                '@SP\nAM=M-1\nD=M\nA=A-1\nD=M-D\n'
                # if (d cmp 0) goto IF_THEN
                f'@CMP_{self._lbl_num:05}_IF_THEN\nD;{op}\n'
                # *(SP-1) = 0;
                '@SP\nA=M-1\nM=0\n'
                # goto ENDIF
                f'@CMP_{self._lbl_num:05}_ENDIF\n0;JMP\n'
                # IF_THEN:
                f'(CMP_{self._lbl_num:05}_IF_THEN)\n'
                # *(SP-1) = -1;
                '@SP\nA=M-1\nM=-1\n'
                # ENDIF:
                f'(CMP_{self._lbl_num:05}_ENDIF)\n'
            )

        if cmd == 'push' and seg == 'constant':
            return self._f.write(
                # *SP = i;
                f'@{i}\nD=A\n@SP\nA=M\nM=D\n'
                # SP++;
                '@SP\nM=M+1\n'
            )

        if cmd == 'push':
            return self._f.write(
                # D = segPtr + i;
                f'{base}\nD={reg}\n@{i}\nA=D+A\nD=M\n'
                # *SP = *addr; SP++;
                '@SP\nA=M\nM=D\n@SP\nM=M+1\n'
            )

        if cmd == 'pop':
            return self._f.write(
                # R13 = segPtr + i;
                f'{base}\nD={reg}\n@{i}\nD=D+A\n@R13\nM=D\n'
                # SP--; D = *SP; *R13 = D;
                '@SP\nAM=M-1\nD=M\n@R13\nA=M\nM=D\n'
            )


def parse(infile, outfile):
    '''parse the HackVM input program and generate Hack ASM code'''
    logger = logging.getLogger('parse()')

    code = Code(outfile)                    # Code Generator instance

    cmnt = re.compile(r'\s*//.*$')          # line comment regex
    idx = re.compile(r'\d+')                # Memory index regex
    cmd_num_args = {
        0: [ 'add','and','eq','gt','lt','neg','not','or','sub', ],
        2: [ 'pop','push', ],
    }
    nargs = { cmd: n for n, cmds in cmd_num_args.items() for cmd in cmds }
    seg_max = {                             # Maximum index for memory segment
        'constant': 0xFFFF, 'pointer': 1, 'static': 256 - 16, 'temp': 7,
        'local': None, 'argument': None, 'this': None, 'that': None,
    }

    for (n, line) in enumerate(infile, start=1):
        outfile.write(f'// {n:05}: {line}')
        ln = cmnt.sub('', line).rstrip('\n')
        logger.debug(f'{n:05}: {ln}')
        if len(ln) == 0: continue
        cmd, *args = ln.split()
        logger.debug(f'{n:05}: cmd="{cmd}", args="{args}".')

        if cmd not in nargs:
            err = f'Unknown command "{cmd}" on line #{n}.'
            logger.error(err)
            raise NameError(err)

        if len(args) > nargs[cmd]:
            err = 'Extra args "{a}" found on line #{n}.'.format(
                a = " ".join(args[nargs[cmd]:]), n=n
            )
            logger.error(err)
            raise TypeError(err)

        if cmd in cmd_num_args[0]:
            code.out(cmd)
            continue

        if idx.match(args[1]) is None:
            err = f'Unknown index "{args[1]}" found on line #{n}.'
            logger.error(err)
            raise TypeError(err)
        else:
            i = int(idx.match(args[1]).group())

        if cmd in ['pop', 'push']:
            # Check that args are valid
            seg = args[0]
            if seg not in seg_max:
                err = f'Unknown segment "{seg}" found on line #{n}.'
                logger.error(err)
                raise KeyError(err)

            if seg_max[seg] and ( i < 0 or i > seg_max[seg] ):
                err = f'Index "{i}" out of bounds for segment "{seg}"' \
                      f' [max: {seg_max[seg]}] on line #{n}.'
                logger.error(err)
                raise IndexError(err)

            if cmd == 'pop' and seg == 'constant':
                err = f'Invalid operation "pop constant" on line #{n}.'
                logger.error(err)
                raise TypeError(err)

            code.out(cmd, seg=seg, i=i)
            continue


if __name__ == '__main__':
    # Read command-line args
    args = get_args()

    # Set up logging
    if args['verbose'] >= 2:
        lvl = logging.DEBUG
    elif args['verbose'] == 1:
        lvl = logging.INFO
    else:
        lvl = logging.WARNING
    logger = logging.basicConfig(stream=args['logfile'], level=lvl)
    logger = logging.getLogger('main()')

    if args['out']:
        outfile = args['out']
    else:
        vm = Path(args['infile'].name)
        if vm.suffix == '.vm':
            out = vm.stem + '.asm'
        else:
            out = vm.name + '.asm'
        logger.info(f'No outfile specified; using "{out}".')
        outfile = open(out, mode='w+')

    parse(args['infile'], outfile)

# vim: set ts=4 sw=4 et si:
