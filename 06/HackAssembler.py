#!/usr/bin/env python3

"""
  HackAssembler.py - assembler for the HACK computer architecture.
  Written by: Joseph Nahmias <joe@nahmias.net>
  For more info, see: https://www.nand2tetris.org/project06
"""

import argparse
import logging
import re
import sys
from datetime import datetime
from pathlib import Path

def get_args():
    """
    Function to parse the command-line arguments according to the
    following rules:
        1) Allow output filename to be specified using -o.
        2) When -o is specified, it must be followed by string to be used as
        the appropriate filename.
        3) If -o is not specified, output filename should default to same as
        the input with the suffix .hack changed/added.
        4) Optional argument -l to generate/write a list file.
    """
    argp = argparse.ArgumentParser(
        allow_abbrev=False,
        description='Assembler for the HACK computer.',
    )
    argp.add_argument(
        '-v', '--verbose',
        action='count', default=0,
        help=('increase log verbosity'),
    )
    argp.add_argument(
        'infile',
        metavar='hack_prog.asm',
        type=argparse.FileType('r'),
        help='name of file to read containing symbolic hack assembly program',
    )
    argp.add_argument(
        '-o', '--out',
        metavar='hack_prog.hack',
        type=argparse.FileType('w'),
        help='name of file to write hack machine language translation'
            + ' [default: change filename extension from .asm to .hack]',
    )
    argp.add_argument(
        '-l', '--list',
        metavar='hack_prog.lst',
        type=argparse.FileType('w'),
        help='name of file for generated assembler list [default: none]',
    )
    argp.add_argument(
        '--logfile',
        metavar='hack_prog.log',
        type=argparse.FileType('w'),
        default=sys.stderr,
        help=('send log messages to the designated file [default: stderr]'),
    )
    return vars(argp.parse_args())

class HackCode:
    """Class to implement Hack binary code."""
    logger = logging.getLogger('HackCode')

    # Parsing elements (re-usable)
    __wsMap = {ord(c):None for c in " \t\n\r\f\v"}  # whitespace characters
    __cmnt = re.compile(r'//.*$')                   # line comments
    __lbl = re.compile(r'^\(([\w.$:]+)\)$')         # jump label
    __ainst = re.compile(r'^@(\d+|[a-zA-Z_.$:][\w.$:]*)$')  # A-instruction
    __cinst = re.compile(r'''^          # Start of line
            ([AMD]+=)?                  # Optional assignment
            ([-+!&|01AMD]{1,3})         # Computation
            (;(J[EGLMN][EPQT]))?        # Optional jump
            $''', re.VERBOSE) # C-instruction

    __predefined_symbols = dict(
        {f'R{i}':i for i in range(16)},             # R0..R15
        SP=0, LCL=1, ARG=2, THIS=3, THAT=4,
        SCREEN=0x4000, KBD=0x6000,
    )

    def __init__(self, symbol_table = __predefined_symbols):
        self.symbols = dict(symbol_table)

    def strip_whitespace_and_comments(self, line):
        ln = line.translate(self.__wsMap)   # strip whitespace
        l = self.__cmnt.sub('', ln)         # strip comments
        return l

    def is_ainst(self, line):
        asm = self.strip_whitespace_and_comments(line)
        return self.__ainst.match(asm) is not None

    def get_addr(self, line):
        asm = self.strip_whitespace_and_comments(line)
        return self.__ainst.match(asm).group(1)

    def is_label(self, line):
        asm = self.strip_whitespace_and_comments(line)
        return self.__lbl.match(asm) is not None

    def get_label(self, line):
        asm = self.strip_whitespace_and_comments(line)
        # Raises an exception if line doesn't contain a label definition
        return self.__lbl.match(asm).group(1)


    # Functions and data for translating Hack C-instructions

    __op_codes = {
        '0':    "0" "101010",
        '1':    "0" "111111",
        '-1':   "0" "111010",
        'D':    "0" "001100",
        'A':    "0" "110000",   'M':    "1" "110000",
        '!D':   "0" "001101",
        '!A':   "0" "110001",  '!M':    "1" "110001",
        '-D':   "0" "001111",
        '-A':   "0" "110011",  '-M':    "1" "110011",
        'D+1':  "0" "011111",
        'A+1':  "0" "110111",  'M+1':   "1" "110111",
        'D-1':  "0" "001110",
        'A-1':  "0" "110010",  'M-1':   "1" "110010",
        'D+A':  "0" "000010",  'D+M':   "1" "000010",
        'D-A':  "0" "010011",  'D-M':   "1" "010011",
        'A-D':  "0" "000111",  'M-D':   "1" "000111",
        'D&A':  "0" "000000",  'D&M':   "1" "000000",
        'D|A':  "0" "010101",  'D|M':   "1" "010101",
    }

    def get_compute_code(self, asm):
        """
        Returns binary string corresponding to compute function.
        If compute is invalid, raises KeyError exception.
        """
        return self.__op_codes[ asm.rpartition('=')[2].partition(';')[0] ]


    def get_assign_code(self, asm):
        """
        Returns binary string corresponding to assignment.
        If destination is invalid, raises KeyError exception.
        """
        a = asm.partition('=')
        if a[1] != '=': return "000"    # no assignment (destination) given
        ds = set(a[0])                  # destination set
        if ds.difference("ADM"): raise KeyError(a[0])
        dest = ""                       # destination code string
        for c in "ADM":
            if c in ds: dest += "1"
            else:       dest += "0"
        return dest


    __jump_codes = { j:f'{i:03b}' for i,j in enumerate(
        [ None, 'JGT', 'JEQ', 'JGE', 'JLT', 'JNE', 'JLE', 'JMP' ]
    ) }

    def get_jump_code(self, asm):
        j = asm.partition(';')
        if j[1] != ';': return self.__jump_codes[None]  # No jump requested
        # Raises KeyError if jump code is invalid (eg. XYZ)
        return self.__jump_codes[j[2]]


    def xlate_cinst(self, line):
        asm = self.strip_whitespace_and_comments(line)
        return "111" + self.get_compute_code(asm) \
            + self.get_assign_code(asm) + self.get_jump_code(asm)

    def valid_cinst(self, line):
        try:
            t = self.xlate_cinst(line)
            return True
        except KeyError:
            return False


def read_labels(f):
    logger = logging.getLogger('read_labels()')

    HC = HackCode()
    errors = 0

    f.seek(0)
    nia = 0     # next instruction address
    for (n, line) in enumerate(f, start=1):

        logger.debug(f'{n:05} = "{line}"')
        l = HC.strip_whitespace_and_comments(line)
        logger.debug(f'{n:05} = "{l}"')

        if len(l) == 0:
            logger.info(f'{n:05}: empty line...skipping.')
            continue
        if HC.is_ainst(l) or HC.valid_cinst(l):
            nia += 1
            logger.info(f'{n:05}: instruction found, nia = {nia:05}.')
            continue

        try:
            label = HC.get_label(l)
            logger.info(f'{n:05}: found label "{label}", nia = {nia:05}.')
            if label not in HC.symbols:
                HC.symbols[label] = nia
            else:
                logger.error(f'Redefinition of label "{label}" on line #{n}.')
                errors += 1
        except:
            logger.error(f'Unrecognized command "{l}" on line #{n}.')
            errors += 1

    logger.info(f'EOF reached; found {nia-1} instructions in {n} lines.')

    if errors:
        sys.exit(f'First pass: {errors} errors. Aborting.')
    return HC.symbols


def output_code(asm_file, symbol_table, outfile, listfile):
    logger = logging.getLogger('read_labels()')
    HC = HackCode(symbol_table)
    errors = 0

    if listfile:
        listfile.write( 'HackAssembler Listfile for assembling'
            +' {i} into {o}.\n'.format(i=asm_file.name, o=outfile.name) )
        listfile.write(datetime.now().astimezone().strftime(
            '%A %d %B %Y %H:%M:%S.%f %Z [%z]\n'))
        listfile.write('\nLoc  Code             LineNo Source\n')

    asm_file.seek(0)
    nva = 16    # next variable address
    nia = 0     # next instruction address
    for (n, line) in enumerate(asm_file, start=1):
        ln = HC.strip_whitespace_and_comments(line)
        if len(ln) == 0 or HC.is_label(ln):
            if listfile:
                listfile.write(' ' *  22)
                listfile.write(f'{n:06} {line}')
            continue
        elif HC.is_ainst(ln):
            a = HC.get_addr(ln)
            if a[0] >= '0' and a[0] <= '9':
                # Literal address
                cmd = f'0{int(a):015b}'
                outfile.write(f'{cmd}\n')
                if listfile:
                    listfile.write(f'{nia:04X} {cmd} {n:06} {line}')
                nia += 1
            else:
                # variable reference
                if a not in HC.symbols:
                    HC.symbols[a] = nva
                    nva += 1
                cmd = f'0{HC.symbols[a]:015b}'
                outfile.write(f'{cmd}\n')
                if listfile:
                    listfile.write(f'{nia:04X} {cmd} {n:06} {line}')
                nia += 1
        elif HC.valid_cinst(ln):
            c = HC.xlate_cinst(ln)
            outfile.write(f'{c}\n')
            if listfile:
                listfile.write(f'{nia:04X} {c} {n:06} {line}')
            nia += 1
        else:
            logger.error(f'Unrecognized command "{l}" on line #{n}.')
            errors += 1

    if errors > 0:
        sys.exit(f'Second pass: {errors} errors. Aborting.')


if __name__ == '__main__':
    # Read command-line args
    args = get_args()

    # Set up logging
    if args['verbose'] >= 2:
        lvl = logging.DEBUG
    elif args['verbose'] == 1:
        lvl = logging.INFO
    else:
        lvl = logging.WARNING
    logger = logging.basicConfig(stream=args['logfile'], level=lvl)
    logger = logging.getLogger('main()')

    if args['out']:
        outfile = args['out']
    else:
        asm = Path(args['infile'].name)
        if asm.suffix == '.asm':
            out = asm.stem + '.hack'
        else:
            out = asm.name + '.hack'
        logger.info(f'No outfile specified; using "{out}".')
        outfile = open(out, mode='w')

    lbls = read_labels(args['infile'])
    #print(lbls)

    output_code(args['infile'], lbls, outfile, args['list'])

# vim: set ts=4 sw=4 et si:
