// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

// Initialization:
//   cur = 0
    @cur
    M = 0
// MAINLOOP:
(MAINLOOP)
//   new = 0
    @new
    M = 0
//   if read kbd > 0 then new = -1
    @KBD
    D = M
    @NOKEYPRESS
    D; JLE
    @new
    M = -1
(NOKEYPRESS)
//   if new - cur = 0 goto MAINLOOP
    @new
    D = M
    @cur
    D = D - M
    @MAINLOOP
    D; JEQ
//   for (block = SCREEN + MAXROWS * MAXCOLS - 1; block >= SCREEN; block--)
//       screen[block] = new
// rewrite these 2 lines as the next 5 lines of pseudo code:
//   block = SCREEN + MAXROWS * MAXCOLS - 1 = 16384 + 256 * 32 - 1 = 24575
    @24575
    D = A
    @block
    M = D
(FILLLOOP)
//   if block - SCREEN < 0 then goto FILLDONE
    @block
    D = M
    @SCREEN
    D = D - A
    @FILLDONE
    D; JLT
//   screen[block] = new
    @new
    D = M
    @block
    A = M
    M = D
//   block--
    @block
    M = M - 1
//   goto FILLLOOP
    @FILLLOOP
    0; JMP
(FILLDONE)
//   cur = new
    @new
    D = M
    @cur
    M = D
//   goto MAINLOOP
    @MAINLOOP
    0; JMP
